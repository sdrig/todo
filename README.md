# ToDo Server

A stand-alone todo server with in-memory database. 

## What is Todo-Server

The project is a PoC for automated deployment from development. 

## Requirements 
 
- GCP account 
- Service Account # need for authorize from gitlab-ci 

### Platforms 

`Linux 5.4.0-31-generic GNU/Linux`

#### Build With

- GoLang 1.14
- Memdb         # github.com/hashicorp/go-memdb
- Gin           #github.com/gin-gonic/gin

### Setting up GCP Account 

- IAM & Admin > Service Account > CREATE SERVICE ACCOUNT
- Fill required field and Create
- Chose __Project Owner__ role 
- Download as JSON format  
\ `On Gitlab`
- Setting>CI-CD>Variables>Add Variable
- Variable name : GCP_SERVICE_ACCOUNT
- Paste your JSON as Value 

### Cluster Setup 

- Create any size of cluster on GCP
- Istio feature must be enabled 

### Variables 
Variables in gitlab-ci.yml must be changed with your new setup's variables and repository path. 

 ```
  PACKAGE_PATH: /go/src/gitlab.com/sdrig/todo
  CLUSTER: todo-cluster
  PROJECT: todo-interview-20
  ZONE: us-central1-f
  GITLAB_IMAGE: registry.gitlab.com/sdrig/todo
  GCR_IMAGE: gcr.io/todo-interview-20/todo-server```

### Web Api 
Web API      | URL           | Description
------------ | ------------- | -------------
Add Todo     | /api/v1/todo  | -
Update Todo  | /api/v1/todo  | -
List Todos   | /api/v1/todo  | -
Delete Todo  | /api/v1/todo  | -

### Clone project
   
```shell
git clone https://gitlab.com/sdrig/todo.git
```
This command will copy files to your local environment.

## 