package test

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/sdrig/todo/database"
	"log"
	"os"
)

// api for test
func InitDb() *database.TaskDB {
	logger := log.New(os.Stdout, "todo-server-test", log.LstdFlags)
	taskDB := database.NewTaskDB(logger, "task_test")
	taskDB.CreateDBIfNotExist(true)
	return taskDB
}
func CreateApiForTest() *gin.Engine {
	app := gin.Default()
	return app
}
