package test

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sdrig/todo/model"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
)

func (suite *TodoTestSuite) TestUpdateTodoItem() {
	suiteT := suite.T()
	suite.Run("Mark done todo item", func() {
		// arrange
		todo := "buy some milk updated"
		var jsonStr = fmt.Sprintf(`{"todo":"%s","done":1,"id":"%s"}`, todo,suite.todoId)
		req, _ := http.NewRequest("PUT", todoRoot, strings.NewReader(jsonStr))
		req.Header.Add("Content-Type", "application/json")
		req.Header.Add("Content-Length", strconv.Itoa(len(jsonStr)))
		// act
		w := httptest.NewRecorder()
		suite.api.ServeHTTP(w, req)
		// assert
		var response *model.Task
		err := json.Unmarshal([]byte(w.Body.String()),&response)
		if err != nil {
			panic(err)
		}
		assert.Equal(suite.T(), 1, response.Done)
	})
	suite.Run("Remove done mark", func() {
		// arrange
		todo := "buy some milk updated"
		var jsonStr = fmt.Sprintf(`{"todo":"%s","done":0,"id":"%s"}`, todo,suite.todoId)
		req, _ := http.NewRequest("PUT", todoRoot, strings.NewReader(jsonStr))
		req.Header.Add("Content-Type", "application/json")
		req.Header.Add("Content-Length", strconv.Itoa(len(jsonStr)))
		// act
		w := httptest.NewRecorder()
		suite.api.ServeHTTP(w, req)
		// assert
		var response *model.Task
		err := json.Unmarshal([]byte(w.Body.String()),&response)
		if err != nil {
			panic(err)
		}
		assert.Equal(suite.T(), 0, response.Done)
	})
	suite.Equal(suiteT, suite.T())
}
